#/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PORT="$1"
AUDIOINDEV=9
AUDIOOUTDEV=9
echo "starting SERVER at " $SCRIPTPATH  "on port " $PORT
pd -nogui -send "server $PORT" -audioindev $AUDIOINDEV -audiooutdev $AUDIOOUTDEV "$SCRIPTPATH"/quacktriper-server.pd
